# JFoenix2

A fork of [JFoenix](https://github.com/jfoenixadmin/JFoenix) with several improvements. Built for/with Java 11, below
might not be working at all.

[JFoenix' License](LICENSE)

## Usable via

[![](https://jitpack.io/v/com.gitlab.grrfe/jfoenix2.svg)](https://jitpack.io/#com.gitlab.grrfe/jfoenix2)

## Changes / features

* `JFXDialog`: add close animation (reverse of default animation)
* `JFXSnackbar`: allow developer to set Y-offset and change X-position (`LEFT`, `CENTER`, `RIGHT`)
* `JFXTextField`: allow developer to specify text-margin (pushes text further to the right inside textfield)
* `JFXPopup`: allow developer to access container (via `getContainer()`, has to be called after `show()` has been
  called) so it can be properly styled

## New components

Add `JFXContainedMaterialButton`, `JFXOutlinedMaterialButton`, `JFXTextMaterialButton`, `JFXFilledTextField`
, `JFXOutlinedTextField`, `JFXFilledComboBox`, `JFXOutlinedComboBox` which look like this (new material design):

![](readme_res/new_components.png)

## Examples

coming soon TM
