plugins {
    kotlin("jvm")
    java
    `maven-publish`
    id("net.nemerosa.versioning")
    id("org.openjfx.javafxplugin") version ("0.1.0")
}

group = "fe.jfoenix2.lib"
version = versioning.info.tag ?: versioning.info.full

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.burningwave:core:12.64.3")
}

javafx {
    version = "21"
    modules = listOf("javafx.controls", "javafx.fxml", "javafx.graphics")
}

kotlin {
    jvmToolchain(21)
}

val jvmArgList = listOf(
    "--add-exports=javafx.controls/com.sun.javafx.scene.control.behavior=com.jfoenix",
    "--add-exports=javafx.base/com.sun.javafx.binding=com.jfoenix",
    "--add-exports=javafx.base/com.sun.javafx.event=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.stage=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.scene=com.jfoenix",
    "--add-exports=javafx.controls/com.sun.javafx.scene.control.behavior=com.jfoenix",
    "--add-exports=javafx.controls/com.sun.javafx.scene.control=com.jfoenix",
    "--add-exports=javafx.controls/com.sun.javafx.scene.control.behavior=com.jfoenix",
    "--add-exports=javafx.base/com.sun.javafx.binding=com.jfoenix",
    "--add-exports=javafx.base/com.sun.javafx.event=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.stage=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.scene=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.geom=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.scene.text=com.jfoenix",
    "--add-exports=javafx.controls/com.sun.javafx.scene.control.inputmap=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.scene.traversal=com.jfoenix",
    "--add-exports=javafx.controls/com.sun.javafx.scene.control=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.util=com.jfoenix",
    "--add-opens=java.base/java.lang.reflect=com.jfoenix",
    "--add-opens=java.base/java.lang.reflect=ALL-UNNAMED",
    "--add-exports=javafx.base/com.sun.javafx.collections=com.jfoenix",
    "--add-opens=javafx.controls/javafx.scene.control.skin=ALL-UNNAMED",
)

tasks.withType<JavaCompile> {
    options.compilerArgs.addAll(jvmArgList)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            version = project.version.toString()

            from(components["java"])
        }
    }
}

