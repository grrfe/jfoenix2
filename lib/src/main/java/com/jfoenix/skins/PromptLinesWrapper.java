/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.jfoenix.skins;

import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.base.IFXLabelFloatControl;
import com.jfoenix.transitions.JFXAnimationTimer;
import com.jfoenix.transitions.JFXKeyFrame;
import com.jfoenix.transitions.JFXKeyValue;
import javafx.animation.Interpolator;
import javafx.beans.InvalidationListener;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableObjectValue;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.WritableValue;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import javafx.util.Duration;

import java.util.function.Supplier;

/**
 * this class used to create label-float/focus-lines for all {@link IFXLabelFloatControl}
 *
 * @author Shadi Shaheen
 * @version 1.0
 * @since 2018-07-19
 */
public class PromptLinesWrapper<T extends Control & IFXLabelFloatControl> {

    private final Supplier<Text> promptTextSupplier;
    private final JFXTextFieldSkin.Type type;
    private final T control;

    public StackPane line = new StackPane();
    public StackPane focusedLine = new StackPane();
    public Pane promptContainer = new Pane();


    private JFXAnimationTimer focusTimer;
    private JFXAnimationTimer unfocusTimer;

    private final double initScale = 0.05;
    public final Scale promptTextScale = new Scale(1, 1, 0, 0);
    private final Scale scale = new Scale(initScale, 1);
    public final Rectangle clip = new Rectangle();

    public ObjectProperty<Paint> animatedPromptTextFill;
    public BooleanBinding usePromptText;
    private final ObjectProperty<Paint> promptTextFill;
    private final ObservableValue<?> valueProperty;
    private final ObservableValue<String> promptTextProperty;

    private boolean animating = false;
    private double contentHeight = 0;
    private double textMargin;

    public PromptLinesWrapper(T control, ObjectProperty<Paint> promptTextFill,
                              ObservableValue<?> valueProperty,
                              ObservableValue<String> promptTextProperty,
                              Supplier<Text> promptTextSupplier) {
        this(control, JFXTextFieldSkin.Type.NONE, promptTextFill, valueProperty, promptTextProperty, promptTextSupplier);
    }

    public PromptLinesWrapper(T control, JFXTextFieldSkin.Type type, ObjectProperty<Paint> promptTextFill,
                              ObservableValue<?> valueProperty,
                              ObservableValue<String> promptTextProperty,
                              Supplier<Text> promptTextSupplier) {
        this.control = control;
        this.type = type;
        this.promptTextSupplier = promptTextSupplier;
        this.promptTextFill = promptTextFill;
        this.valueProperty = valueProperty;
        this.promptTextProperty = promptTextProperty;
    }

    private final Pane borderOverridePane = new Pane();

    public Pane getBorderOverridePane() {
        return borderOverridePane;
    }

    public static final double FLOAT_SCALE_FACTOR = 0.85;
    private static final int ANIMATION_MILLIS = 140;

    public void init(Runnable createPromptNodeRunnable, double textMargin,  Node... cachedNodes) {
        this.init(createPromptNodeRunnable, textMargin, 0, cachedNodes);
    }

    public void init(Runnable createPromptNodeRunnable, double textMargin, double yTranslationAdd, Node... cachedNodes) {
        animatedPromptTextFill = new SimpleObjectProperty<>(promptTextFill.get());
        usePromptText = Bindings.createBooleanBinding(this::usePromptText,
                valueProperty,
                promptTextProperty,
                control.labelFloatProperty(),
                promptTextFill);

        this.textMargin = textMargin;

        // draw lines
        line.setManaged(false);
        line.getStyleClass().add("input-line");
        line.setBackground(new Background(
                new BackgroundFill(control.getUnFocusColor(), CornerRadii.EMPTY, Insets.EMPTY)));

        // focused line
        focusedLine.setManaged(false);
        focusedLine.getStyleClass().add("input-focused-line");
        focusedLine.setBackground(new Background(
                new BackgroundFill(control.getFocusColor(), CornerRadii.EMPTY, Insets.EMPTY)));
        focusedLine.setOpacity(0);
        focusedLine.getTransforms().add(scale);


        if (usePromptText.get()) {
            createPromptNodeRunnable.run();
        }
        usePromptText.addListener(observable -> {
            createPromptNodeRunnable.run();
            control.requestLayout();
        });

        final Supplier<WritableValue<Number>> promptTargetYSupplier =
                () -> promptTextSupplier.get() == null ? null : promptTextSupplier.get().translateYProperty();

        final Supplier<WritableValue<Number>> promptTargetXSupplier =
                () -> promptTextSupplier.get() == null ? null : promptTextSupplier.get().translateXProperty();

        if (this.type == JFXTextFieldSkin.Type.OUTLINED) {
//            this.borderOverridePane.setStyle("-fx-background-color: red");
            this.borderOverridePane.backgroundProperty().bind(control.backgroundProperty());
        }

        focusTimer = new JFXAnimationTimer(
                new JFXKeyFrame(Duration.millis(1),
                        JFXKeyValue.builder()
                                .setTarget(focusedLine.opacityProperty())
                                .setEndValue(1)
                                .setInterpolator(Interpolator.EASE_BOTH)
                                .setAnimateCondition(control::isFocused).build()),

                new JFXKeyFrame(Duration.millis(ANIMATION_MILLIS),
                        JFXKeyValue.builder()
                                .setTarget(scale.xProperty())
                                .setEndValue(1)
                                .setInterpolator(Interpolator.EASE_BOTH)
                                .setAnimateCondition(control::isFocused).build(),
                        JFXKeyValue.builder()
                                .setTarget(animatedPromptTextFill)
                                .setEndValueSupplier(control::getFocusColor)
                                .setInterpolator(Interpolator.EASE_BOTH)
                                .setAnimateCondition(() -> control.isFocused() && control.isLabelFloat()).build(),
                        JFXKeyValue.builder()
                                .setTarget(this.borderOverridePane.translateYProperty())
                                .setEndValueSupplier(() -> -control.getHeight() / 3 + 1)
                                .setAnimateCondition(() -> this.control.isLabelFloat() && this.type == JFXTextFieldSkin.Type.OUTLINED)
                                .setInterpolator(Interpolator.EASE_BOTH).build(),
                        JFXKeyValue.builder()
                                .setTargetSupplier(promptTargetXSupplier)
                                .setEndValueSupplier(clip::getX)
                                .setAnimateCondition(() -> this.type == JFXTextFieldSkin.Type.NONE && control.isLabelFloat())
                                .setInterpolator(Interpolator.EASE_BOTH).build(),
                        JFXKeyValue.builder()
                                .setTargetSupplier(promptTargetYSupplier)
                                .setEndValueSupplier(() ->
                                        this.type == JFXTextFieldSkin.Type.OUTLINED ?
                                                -control.getHeight() / 2 + 2 + yTranslationAdd
                                                : (textMargin > 0 ? -contentHeight + contentHeight / 2 + JFXTextFieldSkin.TRANSLATE_Y * 1.5 : -contentHeight))
                                .setAnimateCondition(control::isLabelFloat)
                                .setInterpolator(Interpolator.EASE_BOTH).build(),
                        JFXKeyValue.builder()
                                .setTarget(promptTextScale.xProperty())
                                .setEndValue(FLOAT_SCALE_FACTOR)
                                .setAnimateCondition(control::isLabelFloat)
                                .setInterpolator(Interpolator.EASE_BOTH).build(),
                        JFXKeyValue.builder()
                                .setTarget(promptTextScale.yProperty())
                                .setEndValue(FLOAT_SCALE_FACTOR)
                                .setAnimateCondition(control::isLabelFloat)
                                .setInterpolator(Interpolator.EASE_BOTH).build())
        );

        unfocusTimer = new JFXAnimationTimer(
                new JFXKeyFrame(Duration.millis(ANIMATION_MILLIS),
                        JFXKeyValue.builder()
                                .setTarget(this.borderOverridePane.translateYProperty())
                                .setEndValue(0)
                                .setAnimateCondition(() -> control.isLabelFloat() && this.type == JFXTextFieldSkin.Type.OUTLINED)
                                .setInterpolator(Interpolator.EASE_BOTH).build(),
                        JFXKeyValue.builder()
                                .setTargetSupplier(promptTargetXSupplier)
                                .setEndValue(0)
                                .setAnimateCondition(() -> this.type == JFXTextFieldSkin.Type.NONE && control.isLabelFloat())
                                .setInterpolator(Interpolator.EASE_BOTH).build(),
                        JFXKeyValue.builder()
                                .setTargetSupplier(promptTargetYSupplier)
                                .setEndValue(0)
                                .setInterpolator(Interpolator.EASE_BOTH).build(),
                        JFXKeyValue.builder()
                                .setTarget(promptTextScale.xProperty())
                                .setEndValue(1)
                                .setInterpolator(Interpolator.EASE_BOTH).build(),
                        JFXKeyValue.builder()
                                .setTarget(promptTextScale.yProperty())
                                .setEndValue(1)
                                .setInterpolator(Interpolator.EASE_BOTH).build())
        );

        promptContainer.getStyleClass().add("prompt-container");
        promptContainer.setManaged(false);
        promptContainer.setMouseTransparent(true);

        // clip prompt container
        clip.setSmooth(false);
        clip.setX(0);

        if (control instanceof JFXTextField) {
            final InvalidationListener leadingListener = obs -> {
                final Node leading = ((JFXTextField) control).getLeadingGraphic();
                if (leading == null) {
                    clip.xProperty().unbind();
                    clip.setX(0);
                } else {
                    clip.xProperty().bind(((Region) leading).widthProperty().negate());
                }
            };
            ((JFXTextField) control).leadingGraphicProperty().addListener(leadingListener);
            leadingListener.invalidated(null);
        }
        clip.widthProperty().bind(promptContainer.widthProperty().add(clip.xProperty().negate()));
        promptContainer.setClip(clip);

        focusTimer.setOnFinished(() -> {
            animating = false;
        });
        unfocusTimer.setOnFinished(() -> {
            animating = false;
        });
        focusTimer.setCacheNodes(cachedNodes);
        unfocusTimer.setCacheNodes(cachedNodes);

        // handle animation on focus gained/lost event
        control.focusedProperty().addListener(observable -> {
            if (control.isFocused()) {
                focus();
            } else {
                unFocus();
            }
        });

        promptTextFill.addListener(observable -> {
            if (!control.isLabelFloat() || !control.isFocused()) {
                animatedPromptTextFill.set(promptTextFill.get());
            }
        });

        updateDisabled();
    }

    public void layoutPrompt(double x, double y, double w, double h) {
        promptContainer.resizeRelocate(x, y, w, h);
        scale.setPivotX(w / 2);
    }

    private Object getControlValue() {
        Object text = valueProperty.getValue();
        text = validateComboBox(text);
        return text;
    }

    private Object validateComboBox(Object text) {
        if (control instanceof ComboBox && ((ComboBox<?>) control).isEditable()) {
            final String editorText = ((ComboBox<?>) control).getEditor().getText();
            text = editorText == null || editorText.isEmpty() ? null : text;
        }
        return text;
    }

    private void focus() {
        unfocusTimer.stop();
        animating = true;
        runTimer(focusTimer, true);
    }

    private void unFocus() {
        focusTimer.stop();
        scale.setX(initScale);
        focusedLine.setOpacity(0);
        if (control.isLabelFloat()) {
            animatedPromptTextFill.set(promptTextFill.get());
            Object text = getControlValue();
            if (text == null || text.toString().isEmpty()) {
                animating = true;
                runTimer(unfocusTimer, true);
            }
        }
    }

    public void updateFocusColor() {
        Paint paint = control.getFocusColor();
        focusedLine.setBackground(paint == null ? Background.EMPTY
                : new Background(new BackgroundFill(paint, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    public void updateUnfocusColor() {
        Paint paint = control.getUnFocusColor();
        line.setBackground(paint == null ? Background.EMPTY
                : new Background(new BackgroundFill(paint, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    private void updateLabelFloat(boolean animation) {
        if (control.isLabelFloat()) {
            if (control.isFocused()) {
                animateFloatingLabel(true, animation);
            } else {
                Object text = getControlValue();
                animateFloatingLabel(!(text == null || text.toString().isEmpty()), animation);
            }
        }
    }

    /**
     * this method is called when the text property is changed when the
     * field is not focused (changed in code)
     *
     * @param up direction of the prompt label
     */
    private void animateFloatingLabel(boolean up, boolean animation) {
        if (promptTextSupplier.get() == null) {
            return;
        }
        if (up) {
            if (promptTextSupplier.get().getTranslateY() != -contentHeight) {
                unfocusTimer.stop();
                runTimer(focusTimer, animation);
            }
        } else {
            if (promptTextSupplier.get().getTranslateY() != 0) {
                focusTimer.stop();
                runTimer(unfocusTimer, animation);
            }
        }
    }

    private void runTimer(JFXAnimationTimer timer, boolean animation) {
        if (animation) {
            if (!timer.isRunning()) {
                timer.start();
            }
        } else {
            timer.applyEndValues();
        }
    }

    private boolean usePromptText() {
        Object txt = getControlValue();
        String promptTxt = promptTextProperty.getValue();
        boolean isLabelFloat = control.isLabelFloat();
        return isLabelFloat || (promptTxt != null
                && (txt == null || txt.toString().isEmpty())
                && !promptTxt.isEmpty()
                && !promptTextFill.get().equals(Color.TRANSPARENT));
    }

    private static final int BORDER_OVERRIDE_PADDING = 3;

    public void layoutBorderOverride(double x, double y, Text promptText) {
        this.borderOverridePane.resizeRelocate(x, y, promptText.getLayoutBounds().getWidth() * FLOAT_SCALE_FACTOR + BORDER_OVERRIDE_PADDING * 2.5,
                promptText.getLayoutBounds().getHeight() * FLOAT_SCALE_FACTOR);
        this.borderOverridePane.setTranslateX(this.textMargin - BORDER_OVERRIDE_PADDING);
    }

    public void layoutLines(double x, double y, double w, double h, double controlHeight, double translateY) {
        this.contentHeight = translateY;
        clip.setY(-contentHeight);
        clip.setHeight(controlHeight + contentHeight);

        this.focusedLine.resizeRelocate(x, controlHeight, w, focusedLine.prefHeight(-1));
        this.line.resizeRelocate(x, controlHeight, w, line.prefHeight(-1));
        this.promptContainer.resizeRelocate(x, y, w, h);

        scale.setPivotX(w / 2);
    }

    public void updateLabelFloatLayout() {
        if (!animating) {
            updateLabelFloat(false);
        } else if (unfocusTimer.isRunning()) {
            // handle the case when changing the control value when losing focus
            unfocusTimer.stop();
            updateLabelFloat(true);
        }
    }

    public void invalid() {
        if (control.isFocused()) {
            focus();
        }
    }

    public void updateDisabled() {
        final boolean disabled = control.isDisable();
        line.setBorder(!disabled ? Border.EMPTY :
                new Border(new BorderStroke(control.getUnFocusColor(),
                        BorderStrokeStyle.DASHED, CornerRadii.EMPTY, new BorderWidths(1))));
        line.setBackground(new Background(
                new BackgroundFill(disabled ? Color.TRANSPARENT : control.getUnFocusColor(), CornerRadii.EMPTY, Insets.EMPTY)));
    }

    public StackPane getLine() {
        return line;
    }

    public Paint getPromptTextFill() {
        return promptTextFill.get();
    }

    public StackPane getFocusedLine() {
        return focusedLine;
    }

    public Pane getPromptContainer() {
        return promptContainer;
    }

    public Boolean getUsePromptText() {
        return usePromptText.get();
    }
}
