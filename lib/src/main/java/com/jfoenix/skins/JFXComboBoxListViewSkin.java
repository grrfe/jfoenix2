/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.jfoenix.skins;

import com.jfoenix.adapters.ReflectionHelper;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.base.IFXLabelFloatControl;
import fe.jfoenix.combobox.JFXFilledComboBox;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.css.CssMetaData;
import javafx.css.Styleable;
import javafx.css.StyleableObjectProperty;
import javafx.css.StyleableProperty;
import javafx.css.converter.PaintConverter;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.skin.ComboBoxListViewSkin;
import javafx.scene.control.skin.TextFieldSkin;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <h1>Material Design ComboBox Skin</h1>
 *
 * @author Shadi Shaheen
 * @version 2.0
 * @since 2017-01-251
 */

public class JFXComboBoxListViewSkin<T> extends ComboBoxListViewSkin<T> {

    private final JFXTextFieldSkin.Type type;
    private final double textMargin;
    private final JFXComboBox<T> comboBox;
    /***************************************************************************
     *                                                                         *
     * Private fields                                                          *
     *                                                                         *
     **************************************************************************/

    private boolean invalid = true;

    private Text promptText;
    private ValidationPane<JFXComboBox<T>> errorContainer;
    private PromptLinesWrapper<JFXComboBox<T>> linesWrapper;

    protected final ObjectProperty<Paint> promptTextFill = new StyleableObjectProperty<>(Color.GRAY) {
        @Override
        public Object getBean() {
            return JFXComboBoxListViewSkin.this;
        }

        @Override
        public String getName() {
            return "promptTextFill";
        }

        @Override
        public CssMetaData<JFXComboBox<?>, Paint> getCssMetaData() {
            return StyleableProperties.PROMPT_TEXT_FILL;
        }
    };

    /***************************************************************************
     *                                                                         *
     * Constructors                                                            *
     *                                                                         *
     **************************************************************************/

    public static final int TRANSLATE_Y = 5;

    public JFXComboBoxListViewSkin(JFXComboBox<T> comboBox) {
        this(comboBox, 0, JFXTextFieldSkin.Type.NONE);
    }

    public JFXComboBoxListViewSkin(final JFXComboBox<T> comboBox, double textMargin, JFXTextFieldSkin.Type type) {
        super(comboBox);

        this.comboBox = comboBox;

        this.type = type;
        this.textMargin = textMargin;

        linesWrapper = new PromptLinesWrapper<>(
                comboBox,
                type,
                promptTextFill,
                comboBox.valueProperty(),
                comboBox.promptTextProperty(),
                () -> promptText);

        linesWrapper.init(this::createPromptNode, textMargin, Y_PUSH_UP);
        Pane arrowButton = null;
        for (Node node : getChildren()) {
            if (node.getId().equals("arrow-button")) {
                arrowButton = (Pane) node;
                break;
            }
        }
        if (arrowButton != null) {
            linesWrapper.clip.widthProperty().bind(linesWrapper.promptContainer.widthProperty().subtract(arrowButton.widthProperty()));
        }

        errorContainer = new ValidationPane<>(comboBox);

        if (type == JFXTextFieldSkin.Type.OUTLINED) {
            comboBox.borderProperty().bind(Bindings.createObjectBinding(
                    () -> new Border(new BorderStroke(comboBox.isFocused() ? comboBox.getFocusColor() : comboBox.getUnFocusColor(),
                            BorderStrokeStyle.SOLID, new CornerRadii(5), new BorderWidths(2))),
                    comboBox.focusedProperty(), comboBox.focusColorProperty()
            ));

            this.getChildren().add(linesWrapper.getBorderOverridePane());
        } else {
            this.getChildren().addAll(linesWrapper.getLine(), linesWrapper.getFocusedLine());
        }

        this.getChildren().addAll(linesWrapper.getPromptContainer(), this.errorContainer);
//        getChildren().addAll(linesWrapper.line, linesWrapper.focusedLine, linesWrapper.promptContainer, errorContainer);

        if (comboBox.isEditable()) {
            comboBox.getEditor().setStyle("-fx-background-color:TRANSPARENT;-fx-padding: 0.333333em 0em;");
            comboBox.getEditor().promptTextProperty().unbind();
            comboBox.getEditor().setPromptText(null);
            comboBox.getEditor().textProperty().addListener((o, oldVal, newVal) -> linesWrapper.usePromptText.invalidate());
        }

        registerChangeListener(comboBox.disableProperty(), obs -> linesWrapper.updateDisabled());
        registerChangeListener(comboBox.focusColorProperty(), obs -> linesWrapper.updateFocusColor());
        registerChangeListener(comboBox.unFocusColorProperty(), obs -> linesWrapper.updateUnfocusColor());
        registerChangeListener(comboBox.disableAnimationProperty(), obs -> errorContainer.updateClip());

        if (type == JFXTextFieldSkin.Type.FILLED && comboBox instanceof JFXFilledComboBox) {
            ListView<T> lv = ReflectionHelper.getFieldContent(ComboBoxListViewSkin.class, this, "listView");
            if (lv != null) {
                lv.setTranslateY(3);
            }

            Node n = getDisplayNode();
            if (n instanceof Region) {
                ((Region) n).maxWidthProperty().bind(comboBox.widthProperty().subtract(textMargin * 2));
                n.setTranslateX(1);
                n.setTranslateY(TRANSLATE_Y);
            }
        }
    }

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    private static final double Y_PUSH_UP = -3;

    @Override
    protected void layoutChildren(final double x, final double y,
                                  final double w, final double h) {
        super.layoutChildren(x, y, w, h);
        final double height = getSkinnable().getHeight();

        if (this.type == JFXTextFieldSkin.Type.OUTLINED) {
            this.linesWrapper.layoutBorderOverride(x, y + 2, this.promptText);
        }
        linesWrapper.layoutLines(x, y, w, h, height, Math.floor(h));

        double yTranslation = y + height / 2 + (promptText != null ? promptText.getLayoutBounds().getHeight() / 2 : 0) + Y_PUSH_UP;
        if (this.type == JFXTextFieldSkin.Type.OUTLINED) {
            yTranslation -= 2;
        }

        linesWrapper.layoutPrompt(x, this.type == JFXTextFieldSkin.Type.NONE ? y : yTranslation, w, h);

//        linesWrapper.layoutLines(x, y, w, h, height,
//                promptText == null ? 0 : snapPositionX(promptText.getBaselineOffset() + promptText.getLayoutBounds().getHeight() * .36));
//        linesWrapper.layoutPrompt(x, y, w, h);
        errorContainer.layoutPane(x, height + linesWrapper.focusedLine.getHeight(), w, h);

        linesWrapper.updateLabelFloatLayout();

        if (invalid) {
            invalid = false;
            // update validation container
            errorContainer.invalid(w);
            // focus
            linesWrapper.invalid();
        }
    }


    private void createPromptNode() {
        if (promptText != null || !linesWrapper.usePromptText.get()) {
            return;
        }

        // create my custom pane for the prompt node
        promptText = new Text();
        promptText.setManaged(false);
        promptText.getStyleClass().add("text");
        promptText.visibleProperty().bind(linesWrapper.usePromptText);
//        promptText.fontProperty().bind(getSkinnable().fontProperty());
        promptText.textProperty().bind(getSkinnable().promptTextProperty());
        promptText.fillProperty().bind(linesWrapper.animatedPromptTextFill);
        promptText.setTranslateX(1);
//
        if (this.textMargin > 0) {
            promptText.setTranslateX(this.textMargin);
        }

        promptText.getTransforms().add(linesWrapper.promptTextScale);
        linesWrapper.promptContainer.getChildren().add(promptText);
        if (getSkinnable().isFocused() && ((IFXLabelFloatControl) getSkinnable()).isLabelFloat()) {
//            promptText.setTranslateY(-Math.floor(getSkinnable().getHeight() + 10));
            linesWrapper.promptTextScale.setX(0.85);
            linesWrapper.promptTextScale.setY(0.85);
        }

//        if (getSkinnable().isFocused() && ((JFXComboBox<T>) getSkinnable()).isLabelFloat()) {
//            promptText.setTranslateY(-snapPositionY(promptText.getBaselineOffset() + promptText.getLayoutBounds().getHeight() * .36));
//            linesWrapper.promptTextScale.setX(0.85);
//            linesWrapper.promptTextScale.setY(0.85);
//        }
    }

    private static class StyleableProperties {
        private static final CssMetaData<JFXComboBox<?>, Paint> PROMPT_TEXT_FILL =
                new CssMetaData<>("-fx-prompt-text-fill",
                        PaintConverter.getInstance(), Color.GRAY) {

                    @Override
                    public boolean isSettable(JFXComboBox n) {
                        final JFXComboBoxListViewSkin<?> skin = (JFXComboBoxListViewSkin<?>) n.getSkin();
                        return skin.promptTextFill == null || !skin.promptTextFill.isBound();
                    }

                    @Override
                    @SuppressWarnings("unchecked")
                    public StyleableProperty<Paint> getStyleableProperty(JFXComboBox n) {
                        final JFXComboBoxListViewSkin<?> skin = (JFXComboBoxListViewSkin<?>) n.getSkin();
                        return (StyleableProperty<Paint>) skin.promptTextFill;
                    }
                };

        private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLES;

        static {
            List<CssMetaData<? extends Styleable, ?>> styleables =
                    new ArrayList<>(ComboBoxListViewSkin.getClassCssMetaData());
            styleables.add(PROMPT_TEXT_FILL);
            STYLEABLES = Collections.unmodifiableList(styleables);
        }
    }


    /**
     * @return The CssMetaData associated with this class, which may include the
     * CssMetaData of its super classes.
     */
    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() {
        return StyleableProperties.STYLEABLES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CssMetaData<? extends Styleable, ?>> getCssMetaData() {
        return getClassCssMetaData();
    }
}
