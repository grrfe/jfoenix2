package fe.jfoenix.combobox;

import com.jfoenix.assets.JFoenixResources;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.skins.JFXComboBoxListViewSkin;
import com.jfoenix.skins.JFXTextFieldSkin;
import javafx.css.CssMetaData;
import javafx.css.SimpleStyleableObjectProperty;
import javafx.css.Styleable;
import javafx.css.StyleableProperty;
import javafx.css.converter.PaintConverter;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Skin;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JFXFilledComboBox<T> extends JFXComboBox<T> {

    private String promptText;

    public JFXFilledComboBox(String promptText) {
        this();
        this.setPromptText(promptText);
    }

    public JFXFilledComboBox() {
        super(42);
        this.getStylesheets().add(JFoenixResources.load("css/filled-combobox.css").toExternalForm());
        this.setLabelFloat(true);
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new JFXComboBoxListViewSkin<>(this,10, JFXTextFieldSkin.Type.FILLED);
    }

    private final SimpleStyleableObjectProperty<Paint> dropdownFill = new SimpleStyleableObjectProperty<>(StyleableProperties.DROPDOWN_FILL,
            JFXFilledComboBox.this,
            "dropdownFill",
            Color.WHITE);

    public SimpleStyleableObjectProperty<Paint> dropdownFillProperty() {
        return dropdownFill;
    }

    public Paint getDropdownFill() {
        return dropdownFill.get();
    }

    public void setDropdownFill(Paint dropdownFill) {
        this.dropdownFill.set(dropdownFill);
    }

    protected static class StyleableProperties {
        private static final CssMetaData<JFXFilledComboBox<?>, Paint> DROPDOWN_FILL = new CssMetaData<>(
                "-jfx-dropdown-fill",
                PaintConverter.getInstance(),
                Color.WHITE) {

            @Override
            public boolean isSettable(JFXFilledComboBox<?> styleable) {
                return styleable.dropdownFill == null || !styleable.dropdownFill.isBound();
            }

            @Override
            public StyleableProperty<Paint> getStyleableProperty(JFXFilledComboBox<?> styleable) {
                return styleable.dropdownFill;
            }
        };

        private static final List<CssMetaData<? extends Styleable, ?>> CHILD_STYLEABLES;

        static {
            final List<CssMetaData<? extends Styleable, ?>> styleables = new ArrayList<>(
                    ComboBox.getClassCssMetaData());
            Collections.addAll(styleables, DROPDOWN_FILL);
            CHILD_STYLEABLES = Collections.unmodifiableList(styleables);
        }
    }

    @Override
    public List<CssMetaData<? extends Styleable, ?>> getControlCssMetaData() {
        return getClassCssMetaData();
    }

    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() {
        return JFXFilledComboBox.StyleableProperties.CHILD_STYLEABLES;
    }
}

