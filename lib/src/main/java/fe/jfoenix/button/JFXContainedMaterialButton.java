package fe.jfoenix.button;

import com.jfoenix.skins.JFXButtonSkin;
import javafx.scene.Node;

public class JFXContainedMaterialButton extends MaterialButtonBase {
    private static final int HORIZONTAL_INSETS = 16 + 3;

    public JFXContainedMaterialButton() {
        this(null, null);
    }

    public JFXContainedMaterialButton(String text, Node graphic) {
        super(HORIZONTAL_INSETS, text, graphic);

        this.setButtonType(ButtonType.RAISED);

        JFXButtonSkin sk = new JFXButtonSkin(this);
        sk.setRaisedDepth(1);
        this.setCustomSkin(sk);
    }
}
