package fe.jfoenix.button;

import javafx.scene.Node;

public class JFXTextMaterialButton extends MaterialButtonBase {
    private static final int HORIZONTAL_INSETS = 11 + 3;

    public JFXTextMaterialButton() {
        this(null, null);
    }

    public JFXTextMaterialButton(String text, Node graphic) {
        super(HORIZONTAL_INSETS, text, graphic);
    }
}
