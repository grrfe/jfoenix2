package fe.jfoenix.button;

import com.jfoenix.assets.JFoenixResources;
import com.jfoenix.controls.JFXButton;
import fe.javafx.fixes.LetterSpacedText;
import javafx.beans.binding.Bindings;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

public abstract class MaterialButtonBase extends JFXButton {
    private static final int HEIGHT = 33;
    private static final Font FONT = Font.font("HK Grotesk SemiBold", 14.2);
    private LetterSpacedText lst;

    public MaterialButtonBase(int horizontalInsets) {
        this(horizontalInsets, null, null);
    }

    public MaterialButtonBase(int horizontalInsets, Node graphic) {
        this(horizontalInsets, null, graphic);
    }

    public MaterialButtonBase(int horizontalInsets, String text, Node graphic) {
        super(text);

        this.lst = new LetterSpacedText(FONT, 0);

        lst.setMaxHeight(HEIGHT);
//        lst.setStyle("-fx-border-color: orange");
        lst.setAlignment(this.getAlignment());
        lst.alignmentProperty().bind(this.alignmentProperty());
        lst.fillProperty().bind(this.textFillProperty());
        lst.textProperty().bind(Bindings.createStringBinding(() -> {
            String t = this.textProperty().get();
            if (t != null) {
                this.textProperty().set(null);
                return t.toUpperCase();
            }

            return null;
        }, this.textProperty()));

        this.setExtraGraphic(graphic);

        this.getStylesheets().add(JFoenixResources.load("css/controls/jfx-material-button.css").toExternalForm());

        this.setMinHeight(HEIGHT);
        this.setMaxHeight(HEIGHT);

        this.prefWidthProperty().bind(Bindings.createDoubleBinding(() -> lst.textWidthProperty().get() + horizontalInsets * 2, lst.textWidthProperty()));
    }

    public void setExtraGraphic(Node graphic) {
        if (graphic != null) {
            HBox box = new HBox(graphic, lst);
//            box.setStyle("-fx-border-color: green");
            box.setAlignment(this.getAlignment());
            box.setSpacing(4);

//            graphic.setStyle("-fx-border-color: blue");
//            lst.setStyle("-fx-border-color: yellow");

            this.setGraphic(box);
        } else {
            this.setGraphic(lst);
        }
    }
}
