package fe.jfoenix.button;

import javafx.beans.binding.Bindings;
import javafx.scene.Node;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;

public class JFXOutlinedMaterialButton extends MaterialButtonBase {
    private static final int HORIZONTAL_INSETS = 16 + 3;

    public JFXOutlinedMaterialButton() {
        this(null, null);
    }

    public JFXOutlinedMaterialButton(String text, Node graphic) {
        super(HORIZONTAL_INSETS, text, graphic);

        this.borderProperty().bind(Bindings.createObjectBinding(() -> new Border(createBorderStroke(this.textFillProperty().get())), this.textFillProperty()));
    }

    public BorderStroke createBorderStroke(Paint paint) {
        return new BorderStroke(paint, BorderStrokeStyle.SOLID, new CornerRadii(4), new BorderWidths(0.8));
    }
}
