package fe.jfoenix.textfield;

import com.jfoenix.assets.JFoenixResources;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.skins.JFXTextFieldSkin;
import javafx.css.SimpleStyleableBooleanProperty;
import javafx.css.StyleableBooleanProperty;
import javafx.scene.control.Skin;

public class JFXFilledTextField extends JFXTextField {
    public JFXFilledTextField(String promptText) {
        this(promptText, null);
    }

    public JFXFilledTextField(String promptText, String text) {
        super(text);

        this.setPromptText(promptText);
        this.getStylesheets().add(JFoenixResources.load("css/filled-textfield.css").toExternalForm());
        super.setLabelFloat(true);
    }

    //add pseudo property to disallow the developer to disable labelFloat
    private final StyleableBooleanProperty labelFloat = new SimpleStyleableBooleanProperty(StyleableProperties.LABEL_FLOAT,
            JFXFilledTextField.this,
            "labelFloat",
            true);

    @Override
    public StyleableBooleanProperty labelFloatProperty() {
        return labelFloat;
    }

    @Override
    public void setLabelFloat(boolean labelFloat) {
        //disallow disabling
    }

    public JFXFilledTextField() {
        this(null);
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new JFXTextFieldSkin<JFXTextField>(this, 10, JFXTextFieldSkin.Type.FILLED);
    }
}
