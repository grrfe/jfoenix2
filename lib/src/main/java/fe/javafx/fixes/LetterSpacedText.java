package fe.javafx.fixes;

import javafx.beans.property.*;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class LetterSpacedText extends HBox {
    private static final String DEFAULT_STYLE_CLASS = "text";

    private final Font font;
    private final StringProperty text = new SimpleStringProperty();
    private final ObjectProperty<Paint> fill = new SimpleObjectProperty<>(Color.BLACK);

    private final DoubleProperty textWidth = new SimpleDoubleProperty(0);

    public LetterSpacedText(Font font, double spacing) {
        this.setSpacing(spacing);
        this.font = font;

        getStyleClass().add(DEFAULT_STYLE_CLASS);

        text.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                this.setText(newValue);
            }
        });
    }

    public void setText(String s) {
        getChildren().clear();

        double updatedWidth = 0;
        for (int i = 0; i < s.length(); i++) {
            Text t = new Text(String.valueOf(s.charAt(i)));
            t.setFill(this.fill.get());
            t.fillProperty().bind(this.fill);

            if (this.font != null) {
                t.setFont(this.font);
            }
            //avoid pushing lots of updates
            updatedWidth += t.getLayoutBounds().getWidth();

            getChildren().add(t);
        }

        this.textWidth.setValue(updatedWidth);
    }


    public DoubleProperty textWidthProperty() {
        return textWidth;
    }

    public StringProperty textProperty() {
        return text;
    }

    public ObjectProperty<Paint> fillProperty() {
        return fill;
    }
}