pluginManagement {
    repositories {
        mavenLocal()
        maven(url = "https://jitpack.io")
        gradlePluginPortal()
    }

    plugins {
        kotlin("jvm") version "1.9.22"
        id("net.nemerosa.versioning") version "3.0.0"
    }
}

rootProject.name = "jfoenix2-multi"

include("lib")
include("testapp")
