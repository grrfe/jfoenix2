plugins {
    kotlin("jvm")
    application
    id("org.openjfx.javafxplugin") version ("0.1.0")
    id("net.nemerosa.versioning")
}

group = "org.example"
version = "1.0-SNAPSHOT"

val mainClassName = "fe.jfoenix2.test.TestApp"

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":lib"))
}

javafx {
    version = "21"
    modules = listOf("javafx.controls", "javafx.fxml", "javafx.graphics")
}

kotlin {
    jvmToolchain(21)
}

//val jvmArgList = listOf("--add-opens=java.base/java.lang.reflect=com.jfoenix")
val jvmArgList = listOf(
    "--add-exports=javafx.controls/com.sun.javafx.scene.control.behavior=com.jfoenix",
    "--add-exports=javafx.base/com.sun.javafx.binding=com.jfoenix",
    "--add-exports=javafx.base/com.sun.javafx.event=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.stage=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.scene=com.jfoenix",
    "--add-exports=javafx.controls/com.sun.javafx.scene.control.behavior=com.jfoenix",
    "--add-exports=javafx.controls/com.sun.javafx.scene.control=com.jfoenix",
    "--add-exports=javafx.controls/com.sun.javafx.scene.control.behavior=com.jfoenix",
    "--add-exports=javafx.base/com.sun.javafx.binding=com.jfoenix",
    "--add-exports=javafx.base/com.sun.javafx.event=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.stage=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.scene=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.geom=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.scene.text=com.jfoenix",
    "--add-exports=javafx.controls/com.sun.javafx.scene.control.inputmap=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.scene.traversal=com.jfoenix",
    "--add-exports=javafx.controls/com.sun.javafx.scene.control=com.jfoenix",
    "--add-exports=javafx.graphics/com.sun.javafx.util=com.jfoenix",
    "--add-opens=java.base/java.lang.reflect=com.jfoenix",
    "--add-opens=java.base/java.lang.reflect=ALL-UNNAMED",
    "--add-exports=javafx.base/com.sun.javafx.collections=com.jfoenix",
    "--add-opens=javafx.controls/javafx.scene.control.skin=ALL-UNNAMED",
    "--add-exports=javafx.controls/com.sun.javafx.scene.control.behavior=ALL-UNNAMED",
    "--add-exports=javafx.base/com.sun.javafx.binding=ALL-UNNAMED",
    "--add-exports=javafx.graphics/com.sun.javafx.stage=ALL-UNNAMED",
    "--add-exports=javafx.base/com.sun.javafx.event=ALL-UNNAMED"
)

application {
    applicationDefaultJvmArgs = jvmArgList
    mainClass.set(mainClassName)
}

//tasks.withType<JavaCompile> {
//    options.compilerArgs.addAll(jvmArgList)
//}
