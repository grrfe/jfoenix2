package fe.jfoenix2.test;

import com.jfoenix.controls.*;
import fe.jfoenix.button.JFXContainedMaterialButton;
import fe.jfoenix.button.JFXOutlinedMaterialButton;
import fe.jfoenix.button.JFXTextMaterialButton;
import fe.jfoenix.combobox.JFXFilledComboBox;
import fe.jfoenix.combobox.JFXOutlinedComboBox;
import fe.jfoenix.textfield.JFXFilledTextField;
import fe.jfoenix.textfield.JFXOutlinedTextField;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import kotlin.Pair;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class TestApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        Font.loadFont(TestApp.class.getResourceAsStream("/hkgrotesk-semibold.ttf"), 14);

//        LetterSpacedText lst = new LetterSpacedText("VIEW ENTRY", Font.font("HK Grotesk SemiBold", 14), 0.0);
//        lst.setFill(Color.WHITE);
//        lst.setMaxHeight(36);
//        lst.setAlignment(Pos.CENTER);
//
//
        JFXContainedMaterialButton btn = new JFXContainedMaterialButton("Contained button", null);
        btn.setAlignment(Pos.CENTER);
        btn.setStyle("-fx-background-color: #6200ee");
        btn.setTextFill(Color.WHITE);
        btn.setRipplerFill(Color.WHITE);
//
        ImageView iv = new ImageView(new Image("file:///home/felix/Desktop/test.png"));

        JFXTextMaterialButton btn2 = new JFXTextMaterialButton("Text button", null);
        btn2.setAlignment(Pos.CENTER);
        btn2.setTextFill(Color.valueOf("#6200ee"));
        btn2.setRipplerFill(Color.valueOf("ba9fea"));


        JFXOutlinedMaterialButton btn3 = new JFXOutlinedMaterialButton("Outlined button", null);
        btn3.setTextFill(Color.valueOf("#6200ee"));
        btn3.setRipplerFill(Color.valueOf("ba9fea"));


//        BorderPane bp = new BorderPane();
//        bp.setLeft(btn);
//        bp.setRight(btn2);
        StackPane sp = new StackPane(new Label("test"));
        Insets insets = new Insets(20, 20, 20, 20);

        StackPane.setMargin(sp, insets);
        JFXPopup popup = new JFXPopup(sp, 300, 700, 0.2);

        JFXListView<String> list = new JFXListView<>();
        list.setItems(FXCollections.observableArrayList("test", "tset2", "test3"));
        list.setOnMouseClicked(evt -> {
//            if(popup.isShowing()){
//                popup.hide();
//            }

            if (evt.getButton() == MouseButton.SECONDARY) {
//                popup.setAutoHide(true);
//                popup.show2(list.getScene().getWindow(), JFXPopup.PopupVPosition.TOP,
//                        JFXPopup.PopupHPosition.LEFT, evt);
                popup.show(list, JFXPopup.PopupVPosition.TOP,
                        JFXPopup.PopupHPosition.LEFT, evt.getX(), evt.getY());

                popup.getContainer().setStyle("-fx-background-color: white; -fx-background-radius: 7; -fx-border-radius: 7");
                popup.getNode().setStyle("-fx-background-color: blue");

                popup.addEventFilter(MouseEvent.MOUSE_CLICKED, x -> {
//                    System.out.println(x.getPickResult());
                    Node n = x.getPickResult().getIntersectedNode();
//                    System.out.println();

                    if (n.getClass().getSimpleName().equals("CSSBridge")) {
                        popup.hide();
                    }
//                    if(n == PopupControl.){
//
//                    }
//                    System.out.println(x.getScreenX() - x.getX() + " " + popup.getAnchorX());
                });
            }
        });


        JFXButton defaultButton = new JFXButton("test");
        defaultButton.setOnRippleFinished(evt -> {
            System.out.println("Code  " + System.currentTimeMillis());

            Platform.runLater(() -> {

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("code2");

            });
        });
        defaultButton.setOnMouseClicked(evt -> {
            System.out.println("Click " + System.currentTimeMillis());
        });

        JFXTextField tf0 = new JFXTextField();
        tf0.setLabelFloat(true);
        tf0.setPromptText("normal");

        JFXFilledTextField tf = new JFXFilledTextField("Filled");
//        tf.setLeadingGraphic(new Pane(new ImageView(new Image(Objects.requireNonNull(TestApp.class.getResourceAsStream("/icon.png"))))));
        tf.setFocusColor(Color.valueOf("#6200ee"));
        tf.setPrefHeight(42);
        tf.setMinHeight(42);

        JFXOutlinedTextField tf2 = new JFXOutlinedTextField("Outlined");
//        tf2.setStyle("-fx-background-color: red");
        tf2.setFocusColor(Color.valueOf("#6200ee"));
        tf2.setPrefHeight(42);
        tf2.setMinHeight(42);
        tf2.setMinWidth(200);

        JFXFilledComboBox<String> cb = new JFXFilledComboBox<>("Filled Comfffffffffffffffffffdsaaaaaaaaaaaaaaaaaaaaaaaaaafbo");
        cb.setFocusColor(Color.valueOf("#6200ee"));
        cb.setPrefHeight(42);
        cb.setMinHeight(42);
//        cb.setMinWidth(200);
        cb.setItems(FXCollections.observableArrayList("test", "x", "test3"));

        JFXOutlinedComboBox<String> cb2 = new JFXOutlinedComboBox<>("Outlined combo");
        tf2.setFocusColor(Color.valueOf("#6200ee"));
        cb2.setPrefHeight(42);
        cb2.setMinHeight(42);
//        cb2.setMinWidth(200);
        cb2.setItems(FXCollections.observableArrayList("test", "test2", "test3"));

        ComboBox<String> test = new ComboBox<>();
        test.setPrefHeight(42);
        test.setMinHeight(42);
        test.setMinWidth(200);
        test.setPromptText("harlo");
        test.setItems(FXCollections.observableArrayList("test", "test2", "test3"));

        JFXComboBox<String> cb3 = new JFXComboBox<>();
        cb3.getStylesheets().add(TestApp.class.getResource("/combobox.css").toExternalForm());

        cb3.setPrefHeight(42);
        cb3.setMinHeight(42);
        cb3.setMinWidth(200);
        cb3.setPromptText("harlo");
        cb3.setItems(FXCollections.observableArrayList("test", "test2", "test3"));

        GridPane gp = new GridPane();
        gp.setStyle("-fx-background-color: white");
        gp.setVgap(25);
        gp.setHgap(15);

        JFXColorPicker picker = new JFXColorPicker();
//        combo.setItems(FXCollections.observableArrayList(List.of("hallo", "test")));
//        gp.getColumnConstraints().addAll(new ColumnConstraints(Region.USE_COMPUTED_SIZE), new ColumnConstraints(Region.USE_COMPUTED_SIZE));
//        gp.
        gp.addRow(0, btn, btn3, btn2);
        gp.addRow(1, tf, tf2);
        gp.addRow(2, cb, cb2);
        gp.addRow(3, cb3);
        gp.addRow(4, picker);
//        gp.getChildren().addAll(btn, btn3, btn2, tf, tf2, cb, cb2);
        gp.setAlignment(Pos.CENTER);

        HBox b = new HBox(
//                btn, btn3, btn2,
//                tf, cb, cb2, tf2
        );
        b.setSpacing(10);
        b.setStyle("-fx-background-color: white");
        b.setAlignment(Pos.CENTER);

        b.setPadding(new Insets(0, 10, 0, 10));

        Pair<JFXSnackbar, HBox> snackbar = makeSnackbar(b, 0);


        btn3.setOnMouseClicked(evt -> {
//            snackbar.getFirst().enqueue(new JFXSnackbar.SnackbarEvent(snackbar.getSecond()));

            popup.show(btn3, JFXPopup.PopupVPosition.TOP,
                    JFXPopup.PopupHPosition.LEFT, evt.getX(), evt.getY());
            popup.getContainer().setStyle("-fx-background-color: white; -fx-background-radius: 7; -fx-border-radius: 7");
        });

        Pair<JFXSnackbar, HBox> snackbar2 = makeSnackbar(b, 10);
        StackPane spRoot = new StackPane(gp);
        JFXDialog dialog = new JFXDialog(spRoot, new StackPane(new Label("test")), JFXDialog.DialogTransition.CENTER, false);

        btn.setOnMouseClicked(evt -> {

            dialog.show();
//            snackbar2.getFirst().enqueue(new JFXSnackbar.SnackbarEvent(snackbar2.getSecond()));
        });


        Scene s = new Scene(spRoot, 1000, 200);
//        s.addEventFilter(MouseEvent.MOUSE_CLICKED, evt -> {
//            System.out.println("click");
//        });


        primaryStage.setScene(s);
        primaryStage.show();

    }

    public Pair<JFXSnackbar, HBox> makeSnackbar(Pane p, int offset) {
        JFXSnackbar snackbar = new JFXSnackbar(p, offset, JFXSnackbar.XLocation.LEFT);
//        snackbar.getStylesheets().add(TestApp.class.getResource("/snackbar.css").toExternalForm());
        Label lb = new Label("test");
        lb.setTextFill(Color.WHITE);
        HBox hb = new HBox(lb);

        return new Pair<>(snackbar, hb);
    }
}
